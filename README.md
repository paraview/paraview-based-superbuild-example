# ParaView Based Superbuild Example

An example of a ParaView Superbuild for a ParaView based application.
This application is intended to be used as an example. It uses ParaView 5.10.1 but
should be compatible with any version of ParaView >= 5.8.0, see below.

It builds, installs and packages an example ParaView based application:
https://gitlab.kitware.com/paraview/simple-paraview-based-application

It has been tested on Windows and Linux.

Developped and maintained by Kitware Europe.

# How to use

This superbuild can be built the same way as the ParaView superbuild, by just configuring and building it.

Linux:
 - Install git and developpement environnement
 - In a terminal:
```
git clone --recursive https://gitlab.kitware.com/paraview/paraview-based-superbuild-example pbse
mkdir build
cd build
cmake ../pbse
make
ctest -R cpack
```

Windows:
 - Install ninja, git and a recent visual studio (>=2015)
 - Install Qt
 - Install NSIS to be able to generate an installer
 - In a visual studio x64 native command:
```
git clone --recursive https://gitlab.kitware.com/paraview/paraview-based-superbuild-example pbse
mkdir build
cd build
cmake ..\pbse -G Ninja
make
ctest -R cpack
```

This will produce an relocatable archive/installer containing SimpleParaView, that can be shared an installed anywhere.

# How to adapt to your own application

You should be able to fork and replace `simpleparaview.*` files by specialized files for your own application,
exactly as if adding a project in the ParaView superbuild.

Please note, only a minimal subset of projects are enabled in the CMakeLists, so if you need other projects
(eg: ospray), you will need to add them.

# How to adapt for other version of ParaView

When changing version of ParaView, the main difference will be the listing of projects. All non-optional projects must be listed
in the main `CMakeLists.txt`. This list will need to be adapted.

Of course, one should also change the version of ParaView in the main [CMakeLists.txt][] (`paraview_SOURCE_SELECTION`) and checkout the right
tag in ParaView-superbuild submodule folder, `pvsb`.

# Implementation logic

The paraview superbuild has been integrated as a git submodule and this project relies on its logic and projects.
The only interesting custom logic is in the main [CMakeLists.txt][], which takes care of enabling the right projects
and configuring the paraview-superbuild as needed.

The remaining files in projects directory take care of compiling and packaging the paraview-based application itself.

# License and Copyright

Copyright (c) 2022 Kitware SAS.

Like ParaView, This ParaView-Superbuild example is distributed under the OSI-approved BSD
3-clause License. See [Copyright.txt][] for details.

[Copyright.txt]: Copyright.txt
[CMakeLists.txt]: CMakeLists.txt
