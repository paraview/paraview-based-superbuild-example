# Only the package name can be changed
# other CPACK variable are set by ParaView directly
# This should be changed in ParaView packaging ideally.
set(CPACK_PACKAGE_NAME "SimpleParaView-1.0-ParaView")

# Package SimpleParaView and all its dependencies
include(paraview.bundle)

set(library_paths "lib")
if (Qt5_DIR)
  list(APPEND library_paths
    "${Qt5_DIR}/../../../bin")
endif ()

# Package the executable
superbuild_windows_install_program("simple_paraview" "bin"
    SEARCH_DIRECTORIES "${library_paths}")

# Package the plugin

set(simpleparaview_plugin_path "bin/SimpleParaView")

install(
  FILES       "${superbuild_install_location}/bin/simple_paraview.conf"
  DESTINATION "bin"
  COMPONENT   "superbuild")

install(
  FILES       "${superbuild_install_location}/${simpleparaview_plugin_path}/SimpleParaView.xml"
  DESTINATION ${simpleparaview_plugin_path}
  COMPONENT   "superbuild")

superbuild_windows_install_plugin("${superbuild_install_location}/bin/SimpleParaView/SimpleParaViewCustomFilters/SimpleParaViewCustomFilters.dll"
  "bin"
  "${simpleparaview_plugin_path}/SimpleParaViewCustomFilters"
  SEARCH_DIRECTORIES "${paraview_plugin_path}/SimpleParaViewCustomFilters" "${library_paths}" "${superbuild_install_location}/bin"
  LOCATION        "${simpleparaview_plugin_path}/SimpleParaViewCustomFilters/")
